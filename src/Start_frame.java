import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;


public class Start_frame extends JFrame {
    private Start_frame frame;

    public Start_frame (){
        this.setTitle("Quotes of teachers");
        this.setSize(900, 600);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);

        Container pane = this.getContentPane();
        pane.setLayout(new GridLayout(1, 1));

        pane.add(buttons());


        this.setVisible(true);
        frame = this;

    }

    private JPanel buttons(){

        JPanel panel = new JPanel();

        JButton guest = new JButton();
        Listener_guest listener_guest = new Listener_guest();
        guest.addActionListener(listener_guest);
        guest.setText("Enter as a guest");

        JButton login = new JButton();
        Listener_login listener_login = new Listener_login();
        login.addActionListener(listener_login);
        login.setText("Log in");

        JButton registration = new JButton();
        Listener_registration listener_registration = new Listener_registration();
        registration.addActionListener(listener_registration);
        registration.setText("Registration");

        panel.setLayout(new GridLayout(3, 1));
        panel.add(login);
        panel.add(guest);
        panel.add(registration);
        return panel;
    }

    private class Listener_guest implements ActionListener{
        public void actionPerformed(ActionEvent e){
            frame.setVisible(false);
            try {
                Quotes_frame quotes_frame = new Quotes_frame(frame);
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    private class Listener_login implements ActionListener{
        public void actionPerformed(ActionEvent e){
            frame.setVisible(false);
            Login_frame login_frame = new Login_frame(frame);
        }
    }

    private class Listener_registration implements ActionListener{
        public void actionPerformed(ActionEvent e){
            frame.setVisible(false);
            Registration_frame registration_frame = new Registration_frame(frame);
        }
    }

}
