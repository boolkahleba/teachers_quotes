import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

public class DeletingQuotes_frame extends JFrame {
    private Menu_frame menu_frame;
    private String login;
    private DeletingQuotes_frame deletingQuotes_frame;
    private int role;
    private Container pane;

    public DeletingQuotes_frame(Menu_frame menu_frame, String login, boolean verifier) throws SQLException {
        this.login = login;
        this.menu_frame = menu_frame;
        this.setTitle("Delete quotes");
        this.setSize(900, 600);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);

        if (verifier){
            role = 2;
        }else {role = 1;}

        pane = this.getContentPane();
        pane.setLayout(new BorderLayout());
        pane.add(return_button(), BorderLayout.NORTH);
        pane.add(fields());

        this.setVisible(true);
        deletingQuotes_frame=this;
    }

    public DeletingQuotes_frame(Menu_frame menu_frame) throws SQLException {
        this.menu_frame = menu_frame;
        this.setTitle("Delete quotes");
        this.setSize(900, 600);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        role = 3;

        pane = this.getContentPane();
        pane.setLayout(new BorderLayout());
        pane.add(return_button(), BorderLayout.NORTH);
        pane.add(fields());

        this.setVisible(true);
        deletingQuotes_frame=this;
    }

    JScrollPane fields() throws SQLException {
        Request request = new Request();
        JPanel jPanel = new JPanel();
        Quotes quotes;
        if (role ==3){
            quotes = new Quotes();
        } else if (role == 2){
            quotes = new Quotes(request.get_group(login));
        }else {
            quotes = new Quotes(request.get_id(login));
        }

        jPanel.setLayout(new GridLayout(quotes.size()+1, 5));
        jPanel.add(new Label("               Цитата"));
        jPanel.add(new Label("       Преподаватель"));
        jPanel.add(new Label("              Предмет"));
        jPanel.add(new Label("                   Дата"));
        jPanel.add(new Label(""));

        for (int i=0; i<quotes.size(); i++){
            Quote quote = (Quote) quotes.get(i);

            jPanel.add(new Label(quote.getQuote()));
            jPanel.add(new Label(quote.getTeacher()));
            jPanel.add(new Label(quote.getSubject()));
            jPanel.add(new Label(quote.getDate()));
            JButton button = new JButton("<--  Delete");
            button.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {

                    try {
                        request.delete_quote(quote.getId());
                    } catch (SQLException ex) {
                        Warning_frame warning_frame = new Warning_frame();
                        ex.printStackTrace();
                    }
                    try {
                        pane.remove(1);
                        pane.add(fields());
                    } catch (SQLException ex) {
                        ex.printStackTrace();
                    }
                    deletingQuotes_frame.revalidate();
                    deletingQuotes_frame.repaint();
                }
            });
            jPanel.add(button);
        }

        return new JScrollPane(jPanel);
    }

    JButton return_button(){
        JButton return_button = new JButton("Return to the Menu page");
        return_button.addActionListener(new Listener_return());
        return return_button;
    }

    private class Listener_return implements ActionListener {
        public void actionPerformed(ActionEvent e){
            deletingQuotes_frame.setVisible(false);
            menu_frame.setVisible(true);
        }
    }
}
