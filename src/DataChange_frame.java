import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

public class DataChange_frame extends JFrame {
    private Container pane;
    private DataChange_frame dataChange_frame;
    private Menu_frame menu_frame;
    private JTextField login;
    private JTextField password;
    private JTextField group;
    private String log;


    public DataChange_frame (Menu_frame menu_frame, String login){
        this.menu_frame = menu_frame;
        this.log = login;
        this.setTitle("Change your personal date");
        this.setSize(900, 600);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);

        pane = this.getContentPane();
        pane.setLayout(new BorderLayout());
        pane.add(button_change(), BorderLayout.SOUTH);
        pane.add(button_return(), BorderLayout.NORTH);
        pane.add(fields());

        this.setVisible(true);
        dataChange_frame = this;
    }

    private JButton button_change(){
        JButton change = new JButton("Change personal data");
        change.addActionListener(new Listener_change());
        return change;
    }

    private JButton button_return(){
        JButton button = new JButton("Return to the Menu page");
        button.addActionListener(new Listener_return());
        return button;
    }

    private JPanel fields(){

        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(3,2));

        login = new JTextField(log);
        password = new JTextField("password");
        group = new JTextField("00-00");

        panel.add(new Label("                                                                                                 Write your new login:"));
        panel.add(login);
        panel.add(new Label("                                                                                              Write your new password:"));
        panel.add(password);
        panel.add(new Label("                                                                                           Write your new study group:"));
        panel.add(group);

        return panel;
    }

    private class Listener_return implements ActionListener {
        public void actionPerformed(ActionEvent e){
            dataChange_frame.setVisible(false);
            menu_frame.setVisible(true);
        }
    }
    private class Listener_change implements ActionListener {
        public void actionPerformed(ActionEvent e){
            Request request = new Request();
            try {
                request.update_user(request.get_id(log), login.getText(), password.getText(), group.getText());
            } catch (SQLException ex) {
                Warning_frame warning_frame = new Warning_frame();
                ex.printStackTrace();
            }
            dataChange_frame.setVisible(false);
            menu_frame.setVisible(true);
            AfterChange_frame afterChange_frame = new AfterChange_frame();
        }
    }
}
