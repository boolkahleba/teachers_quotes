import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

public class Quotes_frame extends  JFrame{
    private Start_frame start_frame;
    private Container pane;
    private Quotes_frame quotes_frame;
    private Menu_frame menu_frame;
    private Quotes quotes;
    private boolean b;

    public Quotes_frame (Start_frame start_frame) throws SQLException { //Вызывается если пользователь вошёл как гость
        this.start_frame = start_frame;
        quotes = new Quotes();
        this.b = b;

        this.setTitle("Quotes");
        this.setSize(900, 600);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);

        pane = this.getContentPane();
        pane.setLayout(new BorderLayout());

        pane.add(quotes());
        pane.add(return_button(false), BorderLayout.SOUTH);

        this.setVisible(true);

        quotes_frame = this;
    }
    public Quotes_frame (Menu_frame menu_frame) throws SQLException { //Вызывается из главного меню
        this.menu_frame = menu_frame;
        quotes = new Quotes();

        this.setTitle("Quotes");
        this.setSize(900, 600);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);

        pane = this.getContentPane();
        pane.setLayout(new BorderLayout());

        pane.add(quotes());
        pane.add(return_button(true), BorderLayout.SOUTH);

        this.setVisible(true);

        quotes_frame = this;
    }

    private JButton return_button(boolean b){

        JButton return_button = new JButton();
        if (b){
            return_button.addActionListener(new Listener_return_menu());
            return_button.setText("Return to the Menu page");
        }else {return_button.addActionListener(new Listener_return_start());
            return_button.setText("Return to the Start page");}

        return return_button;
    }

    private JScrollPane quotes(){
        JPanel jPanel = new JPanel();

        jPanel.setLayout(new GridLayout(quotes.size()+2, 4));
        jPanel.add(new Label("Цитата"));
        jPanel.add(new Label("Преподаватель"));
        jPanel.add(new Label("Предмет"));
        jPanel.add(new Label("Дата"));
        jPanel.add(new Label(""));
        jPanel.add(new Label(""));
        jPanel.add(new Label(""));
        jPanel.add(new Label(""));

        for (int i=0; i<quotes.size(); i++){
            Quote quote = (Quote) quotes.get(i);
            jPanel.add(new Label(quote.getQuote()));
            jPanel.add(new Label(quote.getTeacher()));
            jPanel.add(new Label(quote.getSubject()));
            jPanel.add(new Label(quote.getDate()));
        }

        return new JScrollPane(jPanel);
    }

    private class Listener_return_start implements ActionListener{
        public void actionPerformed(ActionEvent e){
            quotes_frame.setVisible(false);
            start_frame.setVisible(true);
        }
    }

    private class Listener_return_menu implements ActionListener{
        public void actionPerformed(ActionEvent e){
            quotes_frame.setVisible(false);
            menu_frame.setVisible(true);
        }
    }
}

