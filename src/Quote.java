public class Quote {
    private int id;
    private String quote;
    private String teacher;
    private String subject;
    private String date;

    public Quote(int id, String quote, String teacher, String subject, String date){
        this.id = id;
        this.quote = quote;
        this.teacher = teacher;
        this.subject = subject;
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public String getQuote() {
        return quote;
    }

    public String getTeacher() {
        return teacher;
    }

    public String getSubject() {
        return subject;
    }

    public String getDate() {
        return date;
    }
}
