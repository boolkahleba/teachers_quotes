import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Menu_frame extends JFrame {
    private Start_frame start_frame;
    private String login;
    private Menu_frame menu_frame;
    private boolean superUser;


    public Menu_frame(Start_frame start_frame, String login) throws SQLException {
        this.login = login;
        this.start_frame = start_frame;

        this.setTitle("Main menu");
        this.setSize(900, 600);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);

        Container pane = this.getContentPane();
        pane.setLayout(new BorderLayout());

        JButton return_button = new JButton("Return to the Start page");
        return_button.addActionListener(new Listener_return());
        pane.add(return_button, BorderLayout.SOUTH);

        JButton button1 = new JButton("Change personal data");//Изменить логин или пароль
        button1.addActionListener(new Listener_change_personal_data());
        pane.add(button1, BorderLayout.NORTH);

        pane.add(set_functions());
        this.setVisible(true);
        menu_frame = this;
    }

    private JPanel set_functions() throws SQLException {
        JPanel panel1 = new JPanel();

        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(2, 2));
        Request request = new Request();
        int id = request.get_id(login);
        ResultSet resultSet = request.get_functions(id);

        JButton watch_button = new JButton("Watch quotes"); //Просмотр цитат
        watch_button.addActionListener(new Listener_watch_quotes());
        panel.add(watch_button);

        JButton add_quote = new JButton("Add quote"); //Добавление цитат
        add_quote.addActionListener(new Listener_add_quote());
        panel.add(add_quote);

        while (resultSet.next()) {
            JButton button = new JButton();
            int function = resultSet.getInt("f_id");
            if (function == 1) {  //Изменить собственные цитаты
                panel1.setLayout(new GridLayout(1, 1));
                button.setText("Edit my quotes");
                button.addActionListener(new Listener_edit_my_quotes());
                panel.add(button);
            } else if (function == 2) {  //Удалить собственную цитату
                button.setText("Delete my quotes");
                button.addActionListener(new Listener_delete_my_quotes());
                panel.add(button);
                panel1.add(panel);
            } else if (function == 3) {  //Изменить цитаты группы
                panel1.setLayout(new GridLayout(2, 1));
                button.setText("Edit group quotes");
                button.addActionListener(new Listener_edit_group_quotes());
                panel.add(button);
            } else if (function == 4) {  //Удалить цитату группы
                button.setText("Delete group quotes");
                button.addActionListener(new Listener_delete_group_quotes());
                panel.add(button);
                panel1.add(panel);
                JButton delete_user = new JButton("Delete users from group");
                delete_user.addActionListener(new Listener_delete_users());
                panel1.add(delete_user);
                superUser = false;
            } else if (function == 5) {  //Изменить любую цитату
                panel1.setLayout(new GridLayout(2, 1));
                button.setText("Edit any quote");
                button.addActionListener(new Listener_edit_all_quotes());
                panel.add(button);
            } else if (function == 6) {  //Удалить любую цитату
                button.setText("Delete any quote");
                button.addActionListener(new Listener_delete_all_quotes());
                panel.add(button);
                panel1.add(panel);
                JButton delete_user = new JButton("Delete any users");
                delete_user.addActionListener(new Listener_delete_users());
                panel1.add(delete_user);
                superUser = true;
            }
        }
        return panel1;
    }

    private class Listener_watch_quotes implements ActionListener {
        public void actionPerformed(ActionEvent e){
            menu_frame.setVisible(false);
            try {
                Quotes_frame quotes_frame = new Quotes_frame(menu_frame);
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    private class Listener_add_quote implements ActionListener {
        public void actionPerformed(ActionEvent e){
            menu_frame.setVisible(false);
            try {
                AddQuote_frame addQuote_frame = new AddQuote_frame(menu_frame, login);
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    private class Listener_return implements ActionListener {
        public void actionPerformed(ActionEvent e){
            menu_frame.setVisible(false);
            start_frame.setVisible(true);
        }
    }

    private class Listener_edit_my_quotes implements ActionListener {
        public void actionPerformed(ActionEvent e){
            menu_frame.setVisible(false);
            try {
                EditingQuotes_frame editingQuotes_frame = new EditingQuotes_frame(menu_frame, login, false);
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    private class Listener_delete_my_quotes implements ActionListener {
        public void actionPerformed(ActionEvent e){
            menu_frame.setVisible(false);
            try {
                DeletingQuotes_frame deletingQuotes_frame = new DeletingQuotes_frame(menu_frame, login, false);
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    private class Listener_edit_group_quotes implements ActionListener {
        public void actionPerformed(ActionEvent e){
            menu_frame.setVisible(false);
            try {
                EditingQuotes_frame editingQuotes_frame = new EditingQuotes_frame(menu_frame, login, true);
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    private class Listener_delete_group_quotes implements ActionListener {
        public void actionPerformed(ActionEvent e){
            menu_frame.setVisible(false);
            try {
                DeletingQuotes_frame deletingQuotes_frame = new DeletingQuotes_frame(menu_frame, login, true);
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    private class Listener_edit_all_quotes implements ActionListener {
        public void actionPerformed(ActionEvent e){
            menu_frame.setVisible(false);
            try {
                EditingQuotes_frame editingQuotes_frame = new EditingQuotes_frame(menu_frame, login);
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    private class Listener_delete_all_quotes implements ActionListener {
        public void actionPerformed(ActionEvent e){
            menu_frame.setVisible(false);
            try {
                DeletingQuotes_frame deletingQuotes_frame = new DeletingQuotes_frame(menu_frame);
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    private class Listener_change_personal_data implements ActionListener {
        public void actionPerformed(ActionEvent e){
            menu_frame.setVisible(false);
            DataChange_frame dataChange_frame = new DataChange_frame(menu_frame, login);
        }
    }

    private class Listener_delete_users implements ActionListener {
        public void actionPerformed(ActionEvent e){
            if (superUser) {
                menu_frame.setVisible(false);
                try {
                    DeletingUsers_frame deletingUsers_frame = new DeletingUsers_frame(menu_frame);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }else {
                menu_frame.setVisible(false);
                try {
                    DeletingUsers_frame deletingUsers_frame = new DeletingUsers_frame(menu_frame, login);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }


}
