import java.sql.ResultSet;
import java.sql.SQLException;

public class User {
    private int id;
    private String login;
    private String passhash;
    private String group;


    public User(int id, String login, String passhash, String group){
        this.id = id;
        this.login = login;
        this.passhash = passhash;
        this.group = group;
    }

    public int getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }

    public String getPasshash() {
        return passhash;
    }

    public String getGroup() {
        return group;
    }
}
