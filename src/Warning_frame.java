import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Warning_frame extends JFrame {
    private Warning_frame warning_frame;

    public Warning_frame(){
        this.setTitle("WARNING!!!");
        this.setSize(300, 200);
        this.setLocationRelativeTo(null);

        Container pane = this.getContentPane();
        pane.setLayout(new GridLayout(2, 1));
        JButton exit = new JButton("return");
        exit.addActionListener(new Listener_return());
        pane.add(new Label("Wrong data!"));
        pane.add(exit);
        warning_frame = this;
        this.setVisible(true);
    }

    private class Listener_return implements ActionListener {
        public void actionPerformed(ActionEvent e){
            warning_frame.setVisible(false);
        }
    }

}
