import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Quotes extends ArrayList {
    public Quotes() throws SQLException {
        Request request = new Request();
        ResultSet resultSet = request.get_all_quotes();// Все цитаты
        add_quote(resultSet);
    }

    public Quotes (String group) throws SQLException {
        Request request = new Request();
        ResultSet resultSet = request.get_group_quotes(group);// Цитаты группы
        add_quote(resultSet);
    }

    public Quotes (int u_id)throws SQLException{
        Request request = new Request();
        ResultSet resultSet = request.get_user_quotes(u_id);// Цитаты пользователя
        add_quote(resultSet);
    }

    private void add_quote(ResultSet resultSet) throws SQLException {
        while (resultSet.next()){
            add(new Quote(resultSet.getInt("id"),
                    resultSet.getString("quote"),
                    resultSet.getString("teacher"),
                    resultSet.getString("subject"),
                    resultSet.getString("date")));
        }
    }

}
