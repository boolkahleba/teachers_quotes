import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AfterChange_frame extends JFrame {
    private AfterChange_frame afterChange_frame;

    public AfterChange_frame(){
        this.setTitle("");
        this.setSize(200, 100);
        this.setLocationRelativeTo(null);
        Container pane = this.getContentPane();
        pane.setLayout(new GridLayout(2, 1));
        JButton exit = new JButton("OK");
        exit.addActionListener(new Listener_return());
        pane.add(new Label("Changes are successful!"));
        pane.add(exit);
        afterChange_frame = this;
        this.setVisible(true);
    }

    private class Listener_return implements ActionListener {
        public void actionPerformed(ActionEvent e){
            afterChange_frame.setVisible(false);
        }
    }
}
