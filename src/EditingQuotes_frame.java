import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

public class EditingQuotes_frame extends JFrame {
    private Menu_frame menu_frame;
    private String login;
    private EditingQuotes_frame editingQuotes_frame;
    private int role;

    public EditingQuotes_frame(Menu_frame menu_frame, String login, boolean verifier) throws SQLException {
        this.login = login;
        this.menu_frame = menu_frame;
        this.setTitle("Edit quotes");
        this.setSize(900, 600);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);

        if (verifier){
            role = 2;
        }else {role = 1;}

        Container pane = this.getContentPane();
        pane.setLayout(new BorderLayout());
        pane.add(return_button(), BorderLayout.NORTH);
        pane.add(fields());

        this.setVisible(true);
        editingQuotes_frame=this;
    }

    public EditingQuotes_frame(Menu_frame menu_frame, String login) throws SQLException {
        this.login = login;
        this.menu_frame = menu_frame;
        this.setTitle("Edit quotes");
        this.setSize(900, 600);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        role = 3;

        Container pane = this.getContentPane();
        pane.setLayout(new BorderLayout());
        pane.add(return_button(), BorderLayout.NORTH);
        pane.add(fields());

        this.setVisible(true);
        editingQuotes_frame=this;
    }

    JScrollPane fields() throws SQLException {
        Request request = new Request();
        JPanel jPanel = new JPanel();
        Quotes quotes;
        if (role ==3){
            quotes = new Quotes();
        } else if (role == 2){
            quotes = new Quotes(request.get_group(login));
        }else {
            quotes = new Quotes(request.get_id(login));
        }

        jPanel.setLayout(new GridLayout(quotes.size()+1, 5));
        jPanel.add(new Label("               Цитата"));
        jPanel.add(new Label("       Преподаватель"));
        jPanel.add(new Label("              Предмет"));
        jPanel.add(new Label("                   Дата"));
        jPanel.add(new Label(""));

        for (int i=0; i<quotes.size(); i++){
            Quote quote = (Quote) quotes.get(i);

            JTextField q = new JTextField(quote.getQuote());
            JTextField t = new JTextField(quote.getTeacher());
            JTextField s = new JTextField(quote.getSubject());
            JTextField d = new JTextField(quote.getDate());

            jPanel.add(q);
            jPanel.add(t);
            jPanel.add(s);
            jPanel.add(d);
            JButton button = new JButton("<--  Save");
            button.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (!q.getText().isEmpty()&&!t.getText().isEmpty()&&!s.getText().isEmpty()&&!d.getText().isEmpty()) {
                        try {
                            request.update_quote(quote.getId(), q.getText(), t.getText(), s.getText(), d.getText());
                        } catch (SQLException ex) {
                            Warning_frame warning_frame = new Warning_frame();
                            ex.printStackTrace();
                        }
                        editingQuotes_frame.revalidate();
                        editingQuotes_frame.repaint();
                    }else {Warning_frame warning_frame = new Warning_frame();}
                }
            });
            jPanel.add(button);
        }

        return new JScrollPane(jPanel);
    }

    JButton return_button(){
        JButton return_button = new JButton("Return to the Menu page");
        return_button.addActionListener(new Listener_return());
        return return_button;
    }

    private class Listener_return implements ActionListener {
        public void actionPerformed(ActionEvent e){
            editingQuotes_frame.setVisible(false);
            menu_frame.setVisible(true);
        }
    }
}
