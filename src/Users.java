import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Users extends ArrayList {

    public Users() throws SQLException {
        Request request = new Request();
        ResultSet resultSet = request.get_all_users();
        add_users(resultSet);
    }

    public Users(String group) throws SQLException{
        Request request = new Request();
        ResultSet resultSet = request.get_group_users(group);
        add_users(resultSet);
    }

    private void add_users(ResultSet resultSet) throws SQLException {
        while (resultSet.next()){
            add(new User(resultSet.getInt("id"),
                    resultSet.getString("login"),
                    resultSet.getString("passhash"),
                    resultSet.getString("study_group")));
        }
    }
}
