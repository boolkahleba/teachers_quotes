import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

public class Registration_frame extends JFrame{
    Registration_frame registration_frame;
    Start_frame start_frame;
    JTextField login;
    JTextField password;
    JTextField group;

    public Registration_frame (Start_frame start_frame){
        this.start_frame = start_frame;
        this.setTitle("Registration");
        this.setSize(900, 600);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);

        Container pane = this.getContentPane();
        pane.setLayout(new BorderLayout());
        pane.add(registration_button(), BorderLayout.SOUTH);
        pane.add(return_button(), BorderLayout.NORTH);
        pane.add(fields());

        this.setVisible(true);
        registration_frame = this;

    }
    JButton return_button(){
        JButton return_button = new JButton("Return to the Start page");
        return_button.addActionListener(new Listener_return());
        return return_button;
    }

    JButton registration_button(){
        JButton registration = new JButton("Registrate");
        registration.addActionListener(new Listener_registration());
        return registration;
    }

    private JPanel fields(){

        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(3,2));

        login = new JTextField();
        password = new JTextField();
        group = new JTextField();

        panel.add(new Label("                                                                                                 Write your login:"));
        panel.add(login);
        panel.add(new Label("                                                                                              Write your password:"));
        panel.add(password);
        panel.add(new Label("                                                                                           Write your study group:"));
        panel.add(group);


        return panel;
    }

    private class Listener_return implements ActionListener {
        public void actionPerformed(ActionEvent e){
            registration_frame.setVisible(false);
            start_frame.setVisible(true);
        }
    }
    private class Listener_registration implements ActionListener {
        public void actionPerformed(ActionEvent e){
            Request request = new Request();
            if (!login.getText().isEmpty()&&!password.getText().isEmpty()&&!group.getText().isEmpty()) {
                try {
                    request.add_user(login.getText(), password.getText(), group.getText());
                    int id = request.get_id(login.getText());
                    request.add_function(id, 1);
                    request.add_function(id, 2);
                } catch (SQLException ex) {
                    Warning_frame warning_frame = new Warning_frame();
                    ex.printStackTrace();
                }
                registration_frame.setVisible(false);
                start_frame.setVisible(true);
            }else {
                Warning_frame warning_frame = new Warning_frame();
            }
        }
    }
}
