import java.sql.*;

public class Request {
    private Connection connection;
    public Request(){
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            connection = DriverManager.getConnection(
                    "jdbc:mysql://std-mysql.ist.mospolytech.ru:3306/std_2017_quotes_of_teachers",
                    "std_2017_quotes_of_teachers", "Artem080303");
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public ResultSet get_all_quotes() throws SQLException {
        PreparedStatement statement = connection.prepareStatement("SELECT * FROM quotes");
        ResultSet resultSet = statement.executeQuery();
        return resultSet;
    }

    public ResultSet get_group_quotes(String group) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(
                "SELECT * FROM quotes WHERE u_id IN (SELECT id FROM users WHERE study_group = ?)");
        statement.setString(1, group);
        ResultSet resultSet = statement.executeQuery();
        return resultSet;
    }

    public ResultSet get_user_quotes(int u_id) throws SQLException {
        PreparedStatement statement = connection.prepareStatement("SELECT * FROM quotes WHERE u_id = ?");
        statement.setInt(1, u_id);
        ResultSet resultSet = statement.executeQuery();
        return resultSet;
    }

    public ResultSet get_all_users() throws SQLException {
        PreparedStatement statement = connection.prepareStatement("SELECT * FROM users");
        ResultSet resultSet = statement.executeQuery();
        return resultSet;
    }

    public ResultSet get_group_users(String group) throws SQLException {
        PreparedStatement statement = connection.prepareStatement("SELECT * FROM users WHERE study_group = ?");
        statement.setString(1, group);
        ResultSet resultSet = statement.executeQuery();
        return resultSet;
    }

    public void add_user(String login, String password, String group) throws SQLException{
        password = String.valueOf(password.hashCode());
        PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO users (login, passhash, study_group) VALUES (?, ?, ?)");
        preparedStatement.setString(1, login);
        preparedStatement.setString(2, password);
        preparedStatement.setString(3, group);

        int rows = preparedStatement.executeUpdate();

        System.out.printf("%d rows added", rows);
        System.out.println();
    }

    public void add_quote(int u_id, String quote, String teacher, String subject, String date) throws SQLException{
        PreparedStatement preparedStatement = connection.prepareStatement(
                "INSERT INTO quotes (quote, teacher, subject, date, u_id) VALUES (?, ?, ?, ?, ?)");
        preparedStatement.setString(1, quote);
        preparedStatement.setString(2, teacher);
        preparedStatement.setString(3, subject);
        preparedStatement.setString(4, date);
        preparedStatement.setInt(5, u_id);

        int rows = preparedStatement.executeUpdate();
        System.out.printf("%d rows added", rows);
        System.out.println();
    }

    public void update_quote(int q_id, String quote, String teacher, String subject, String date) throws SQLException{
        PreparedStatement preparedStatement = connection.prepareStatement(
                "UPDATE quotes SET quote=?, teacher=?, subject=?, date=? WHERE id = ?");
        preparedStatement.setString(1, quote);
        preparedStatement.setString(2, teacher);
        preparedStatement.setString(3, subject);
        preparedStatement.setString(4, date);
        preparedStatement.setInt(5, q_id);

        int rows = preparedStatement.executeUpdate();
        System.out.printf("%d rows updated", rows);
        System.out.println();
    }

    public void delete_user(String login) throws SQLException{
        PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM users WHERE login = ?");
        preparedStatement.setString(1, login);

        int rows = preparedStatement.executeUpdate();

        System.out.printf("%d rows deleted", rows);
        System.out.println();
    }

    public void delete_quote(int q_id) throws SQLException{
        PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM quotes WHERE id = ?");
        preparedStatement.setInt(1, q_id);

        int rows = preparedStatement.executeUpdate();

        System.out.printf("%d rows deleted", rows);
        System.out.println();
    }

    public void add_function(int user_id, int function_id) throws SQLException{
        PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO users_functions VALUES (?, ?)");
        preparedStatement.setInt(1, user_id);
        preparedStatement.setInt(2, function_id);

        int rows = preparedStatement.executeUpdate();

        System.out.printf("%d rows added", rows);
        System.out.println();
    }

    public ResultSet get_functions(int id)throws SQLException{
        PreparedStatement statement = connection.prepareStatement("SELECT f_id FROM users_functions WHERE u_id=? ORDER BY f_id");
        statement.setInt(1, id);
        ResultSet resultSet = statement.executeQuery();
        return resultSet;
    }

    public String get_passhash (String login) throws SQLException {
        PreparedStatement statement = connection.prepareStatement("SELECT passhash FROM users WHERE login=?");
        statement.setString(1, login);
        ResultSet resultSet = statement.executeQuery();
        resultSet.next();
        return resultSet.getString("passhash");
    }

    public int get_id (String login) throws SQLException {
        PreparedStatement statement = connection.prepareStatement("SELECT id FROM users WHERE login=?");
        statement.setString(1, login);
        ResultSet resultSet = statement.executeQuery();
        resultSet.next();
        return resultSet.getInt("id");
    }

    public String get_group (String login) throws SQLException {
        PreparedStatement statement = connection.prepareStatement("SELECT study_group FROM users WHERE login=?");
        statement.setString(1, login);
        ResultSet resultSet = statement.executeQuery();
        resultSet.next();
        return resultSet.getString("study_group");
    }

    public void update_user(int id, String login, String password, String group)throws SQLException{
        password = String.valueOf(password.hashCode());
        PreparedStatement preparedStatement = connection.prepareStatement("UPDATE users SET login = ?, passhash = ?, study_group = ? WHERE users.id = ?");
        preparedStatement.setString(1, login);
        preparedStatement.setString(2, password);
        preparedStatement.setString(3, group);
        preparedStatement.setInt(4, id);

        int rows = preparedStatement.executeUpdate();

        System.out.printf("%d rows updated", rows);
        System.out.println();
    }
}
