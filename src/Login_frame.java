import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

public class Login_frame extends JFrame{

    Login_frame login_frame;
    Start_frame start_frame;
    JTextField login;
    JTextField password;

    public Login_frame (Start_frame start_frame){
        this.start_frame = start_frame;
        this.setTitle("Login");
        this.setSize(900, 600);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);

        Container pane = this.getContentPane();
        pane.setLayout(new BorderLayout());
        pane.add(login_button(), BorderLayout.SOUTH);
        pane.add(return_button(), BorderLayout.NORTH);
        pane.add(fields());

        this.setVisible(true);
        login_frame = this;

    }

    JButton return_button(){
        JButton return_button = new JButton("Return to the Start page");
        return_button.addActionListener(new Listener_return());
        return return_button;
    }
    JButton login_button(){
        JButton return_button = new JButton("Login");
        return_button.addActionListener(new Listener_login());
        return return_button;
    }

    private JPanel fields(){

        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(2, 2));

        login = new JTextField();
        password = new JTextField();

        panel.add(new Label("                                                                                                 Write your login:"));
        panel.add(login);
        panel.add(new Label("                                                                                              Write your password:"));
        panel.add(password);

        return panel;
    }

    private class Listener_return implements ActionListener {
        public void actionPerformed(ActionEvent e){
            login_frame.setVisible(false);
            start_frame.setVisible(true);
        }
    }
    private class Listener_login implements ActionListener {
        public void actionPerformed(ActionEvent e){
            Request request = new Request();
            try {
                if (String.valueOf(password.getText().hashCode()).equals(request.get_passhash(login.getText()))){
                    login_frame.setVisible(false);
                    Menu_frame menu_frame = new Menu_frame(start_frame, login.getText());
                }else {
                    Warning_frame warning_frame = new Warning_frame();
                }
            } catch (SQLException ex) {
                Warning_frame warning_frame = new Warning_frame();
                ex.printStackTrace();
            }
        }
    }
}
