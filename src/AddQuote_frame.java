import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;

public class AddQuote_frame extends JFrame {
    AddQuote_frame addQuote_frame;
    Menu_frame menu_frame;
    JTextField quote;
    JTextField teacher;
    JTextField subject;
    JTextField date;
    String login;

    public AddQuote_frame (Menu_frame menu_frame, String login) throws SQLException {
        this.login = login;
        this.menu_frame = menu_frame;
        this.setTitle("Add Quote");
        this.setSize(900, 600);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);

        Container pane = this.getContentPane();
        pane.setLayout(new BorderLayout());
        pane.add(add_quote_button(), BorderLayout.SOUTH);
        pane.add(return_button(), BorderLayout.NORTH);
        pane.add(fields());

        this.setVisible(true);
        addQuote_frame = this;

    }
    JButton return_button(){
        JButton return_button = new JButton("Return to the Menu page");
        return_button.addActionListener(new Listener_return());
        return return_button;
    }

    JButton add_quote_button(){
        JButton add = new JButton("Add quote");
        add.addActionListener(new Listener_add());
        return add;
    }

    private JPanel fields() throws SQLException {
        Request request = new Request();

        JPanel panel1 = new JPanel();
        panel1.setLayout(new BorderLayout());

        int count = 0;
        ResultSet resultSet = request.get_user_quotes(request.get_id(login));
        while (resultSet.next()){
            count++;
        }
        JTextField textField = new JTextField();

        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(4,2));

        quote = new JTextField("");
        teacher = new JTextField("Иванов И. И.");
        subject = new JTextField();
        date = new JTextField("2022-07-13");

        panel.add(new Label("                                                                                                     Quote:"));
        panel.add(quote);
        panel.add(new Label("                                                                                              Teacher name:"));
        panel.add(teacher);
        panel.add(new Label("                                                                                                   Subject:"));
        panel.add(subject);
        panel.add(new Label("                                                                                                      Date:"));
        panel.add(date);

        panel1.add(panel);
        panel1.add(new Label("                  Count of your quotes: "+ count), BorderLayout.SOUTH);
        return panel1;
    }

    private class Listener_return implements ActionListener {
        public void actionPerformed(ActionEvent e){
            addQuote_frame.setVisible(false);
            menu_frame.setVisible(true);
        }
    }
    private class Listener_add implements ActionListener {
        public void actionPerformed(ActionEvent e){
            Request request = new Request();
            if (!quote.getText().isEmpty()&&!teacher.getText().isEmpty()&&!subject.getText().isEmpty()&&!date.getText().isEmpty()) {
                try {
                    request.add_quote(request.get_id(login), quote.getText(), teacher.getText(), subject.getText(), date.getText());
                } catch (SQLException ex) {
                    Warning_frame warning_frame = new Warning_frame();
                    ex.printStackTrace();
                }
                addQuote_frame.setVisible(false);
                menu_frame.setVisible(true);
            }else {Warning_frame warning_frame = new Warning_frame();}
        }
    }
}
