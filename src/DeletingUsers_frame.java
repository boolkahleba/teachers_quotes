import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

public class DeletingUsers_frame extends JFrame {
    private Menu_frame menu_frame;
    private String login;
    private DeletingUsers_frame deletingUsers_frame;
    private boolean superUser;
    private Container pane;

    public DeletingUsers_frame(Menu_frame menu_frame, String login) throws SQLException {
        this.login = login;
        this.menu_frame = menu_frame;
        this.setTitle("Delete quotes");
        this.setSize(900, 600);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        superUser = false;

        pane = this.getContentPane();
        pane.setLayout(new BorderLayout());
        pane.add(return_button(), BorderLayout.NORTH);
        pane.add(fields());

        this.setVisible(true);
        deletingUsers_frame=this;
    }

    public DeletingUsers_frame(Menu_frame menu_frame) throws SQLException {
        this.menu_frame = menu_frame;
        this.setTitle("Delete users");
        this.setSize(900, 600);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        superUser = true;

        pane = this.getContentPane();
        pane.setLayout(new BorderLayout());
        pane.add(return_button(), BorderLayout.NORTH);
        pane.add(fields());

        this.setVisible(true);
        deletingUsers_frame=this;
    }

    JScrollPane fields() throws SQLException {
        Request request = new Request();
        JPanel jPanel = new JPanel();
        Users users;

        if(superUser){
            users = new Users();
        }else {
            users = new Users(request.get_group(login));
        }
        if (superUser) {
            jPanel.setLayout(new GridLayout(users.size() - 1, 4));
        }else {jPanel.setLayout(new GridLayout(users.size(), 4));}
        jPanel.add(new Label("              ID"));
        jPanel.add(new Label("     Логин"));
        jPanel.add(new Label("     Группа"));
        jPanel.add(new Label(""));

        for (int i=0; i<users.size(); i++){
            User user = (User) users.get(i);

            if ((
                    superUser&&!user.getGroup().equals("SuperUser"))||
                    !superUser&&!user.getGroup().equals("SuperUser")&&!user.getLogin().equals(login)) {

                jPanel.add(new Label("           " + user.getId()));
                jPanel.add(new Label(user.getLogin()));
                jPanel.add(new Label(user.getGroup()));
                JButton button = new JButton("<--  Delete");
                button.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {

                        try {
                            request.delete_user(user.getLogin());
                        } catch (SQLException ex) {
                            Warning_frame warning_frame = new Warning_frame();
                            ex.printStackTrace();
                        }
                        pane.remove(1);
                        try {
                            pane.add(fields());
                        } catch (SQLException ex) {
                            ex.printStackTrace();
                        }
                        deletingUsers_frame.revalidate();
                        deletingUsers_frame.repaint();
                    }
                });
                jPanel.add(button);
            }
        }

        return new JScrollPane(jPanel);
    }

    JButton return_button(){
        JButton return_button = new JButton("Return to the Menu page");
        return_button.addActionListener(new Listener_return());
        return return_button;
    }

    private class Listener_return implements ActionListener {
        public void actionPerformed(ActionEvent e){
            deletingUsers_frame.setVisible(false);
            menu_frame.setVisible(true);
        }
    }
}
